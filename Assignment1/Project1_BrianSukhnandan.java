/* Brian Sukhnandan */

import java.util.*;

/**
 * Brian Sukhnandan
 * CS381 - Modeling & Simulation
 *
 * Write a program that simulates a single server fcfs queue. There should be a
 * future events list and a delayed events list as part of the program. There are
 * two types of events, an end of service and a next arrival, both types (if they
 * exist) on the future events list. Jobs that are delayed because they arrive while
 * the server is busy are put on the delayed list, in arrival order. On avg 10 jobs
 * arrival a minute (every 6 seconds) and the servicing requires 5 seconds on avg.
 * Both arrivals and servicing have an exponential distribution. Run the program
 * for 1000 service completions.
 *
 * The results should include the avg inter-arrival times, the avg service amount,
 * and the first 20 job's arrival and service requirements (these are checks on your
 * program). Also output are the avg busy period of the server, the number of busy
 * periods and the avg wait time of jobs.
 * */

public class Project1_BrianSukhnandan {

    // Computes exponential function, where inter-arrival (lambda) time is 6 seconds.
    // So the formula would be (-1/6)*log(R). Then multiply by 100 to convert to seconds.
    public static double getInterarrivalTime () {
        Random r = new Random();
        return (-6)*(Math.log(r.nextDouble()));
    }

    // Computes exponential function, where Service (lambda) time is 5 seconds.
    // So the formula would be (-1/5)*log(R). Then multiply by 100 to convert to seconds.
    public static double getServiceTime () {
        Random r = new Random();
        return (-5)*(Math.log(r.nextDouble()));
    }

    public static void printInfo(ArrayList<Job> listOfJobs, int i) {

        System.out.println("Job " + (i+1) + ": ");
        System.out.println("Arrival time: "+listOfJobs.get(i).arrivalTime);
        System.out.println("Departure time: "+listOfJobs.get(i).departureTime);
        System.out.println("Service time: "+listOfJobs.get(i).serviceTime);
        System.out.println("Delay time: "+listOfJobs.get(i).delayTime);

        System.out.println();

    }

    public static void main(String args[]) {

        System.out.println("\nBrian Sukhnandan");
        System.out.println("CS381 - Modeling & Simulation");

        // Initialize Queues for Events.
        Queue<Job> FutureEvents = new LinkedList<Job>();
        Queue<Job> DelayedEvents = new LinkedList<Job>();

        ArrayList<Job> listOfJobs = new ArrayList<Job>();

        // Array to hold inter-arrival times.
        double[] interArrivalTimes = new double[1000];

        // Initialize clock time.
        double clockTime = 0.0;

        // Initialize number of jobs.
        int numJobs = 0;

        for (numJobs = 0; numJobs < 1000; numJobs++) {

            // Create random variables for interarrival time, and service time
            // for each job.
            double InitialirTime = getInterarrivalTime();
            double serviceTime = getServiceTime();

            // If we aren't on the first job and the current clock time is less
            // than the previous job's departure time,
            // Create and Add next job to FutureEvents.
            if ((numJobs > 0) && (clockTime < listOfJobs.get(numJobs-1).departureTime)) {

                // Declare a job in this scope to be edited.
                Job j;

                // If there are no future events, create a new job that should start at the
                // current clock time, which will be
                // += irTime from the previous increment, and the departure time should be
                // The previous job's departure time + service Time.
                if (FutureEvents.isEmpty()) {

                    j = new Job(clockTime, listOfJobs.get(numJobs-1).departureTime+serviceTime, serviceTime, listOfJobs.get(numJobs-1).departureTime-clockTime);
                    FutureEvents.add(j);

                    // Add job to listOfJobs.
                    listOfJobs.add(j);

                    // Calculate the inter-arrival times between each event.
                    interArrivalTimes[numJobs-1] = listOfJobs.get(numJobs).arrivalTime - listOfJobs.get(numJobs-1).arrivalTime;

                    // After we are done processing, remove it from the queue.
                    FutureEvents.remove(j);

                }

                // If there are future events however,
                else {

                    // Make j equal to the next thing in queue.
                    j = FutureEvents.peek();

                    // Add job to listOfJobs.
                    listOfJobs.add(j);

                    // Calculate the inter-arrival times between each event.
                    interArrivalTimes[numJobs-1] = listOfJobs.get(numJobs).arrivalTime - listOfJobs.get(numJobs-1).arrivalTime;

                    // After we are done processing, remove it from the queue.
                    FutureEvents.remove(j);

                }

                // List of jobs that will be delayed will be in this Queue.
                DelayedEvents.add(j);

                // This next job should start at the current clock time, which will be
                // += irTime from the previous increment, and the departure time should be
                // The previous job's departure time + service Time.

                clockTime += interArrivalTimes[numJobs-1];
            }

            else {

                // Else Create a job and add it to the FutureEvents queue.
                // Let the arrival Time be the current time + interarrival time.
                // Then let the departure time be that time + 5 seconds after

                // The delay time for this specific job is 0, because the server is not busy.
                Job j = new Job(clockTime, clockTime+serviceTime, serviceTime, 0);

                listOfJobs.add(j);

                // Update the clock to be the time when the next job comes in.
                clockTime += InitialirTime;
            }

        }

        System.out.println();
        for (int i = 0; i < 20; i++) {
            printInfo(listOfJobs, i);
        }

        System.out.println();

        // Calculate avg inter-arrival time.
        double avgInterarrivalTime = 0.0;
        for (int i = 0; i < 1000; i++) {
            avgInterarrivalTime += interArrivalTimes[i];
        }
        avgInterarrivalTime /= 1000;

        // Calculate avg delay time.
        double avgDelayTime = 0.0;
        for (int i = 0; i < 1000; i++) {
            avgDelayTime += listOfJobs.get(i).delayTime;
        }
        avgDelayTime /= 1000;

        double avgServiceTime = 0.0;
        for (int i = 0; i < 1000; i++) {
            avgServiceTime += listOfJobs.get(i).serviceTime;
        }
        avgServiceTime /= 1000;

        int numBusyPeriods = 0;
        for (int i = 0; i < 1000; i++) {
            if (listOfJobs.get(i).delayTime == 0) { numBusyPeriods++; }
        }

        double avgBusyPeriod = 0.0;

        // Variable to store index of last time delay time was 0.
        int j = 0;
        int k = 0;
        for (int i = 1; i < 1000; i++) {

            // If there is only one busy time, take the time in which the last job ends.
            if (numBusyPeriods <= 1) {
                avgBusyPeriod = listOfJobs.get(listOfJobs.size()-1).departureTime;
            }

            // If we haven't found all busy periods.
            else if ((numBusyPeriods > 1) && (k != numBusyPeriods)){

                // If the delay time of a job is 0.
                if (listOfJobs.get(i).delayTime == 0.0) {

                    // Increment our counter.
                    ++k;

                    // Get difference in arrival times of both jobs with a delay time of 0.
                    avgBusyPeriod += listOfJobs.get(i).arrivalTime - listOfJobs.get(j).arrivalTime;

                    // Then set our j counter to prev. value of i.
                    j = i;

                }

            }

        }
        // Lastly, get the last interval of busy period.
        avgBusyPeriod += listOfJobs.get(listOfJobs.size()-1).arrivalTime - listOfJobs.get(j).arrivalTime;
        avgBusyPeriod /= numBusyPeriods;

        System.out.println("Average Inter-arrival time: " + avgInterarrivalTime + " seconds.");
        System.out.println("Average Delay time (wait time): " + avgDelayTime + " seconds.");
        System.out.println("Average Service time: " + avgServiceTime + " seconds.");
        System.out.println("Average Busy time of server: " + avgBusyPeriod + " seconds.");
        System.out.println("Number of busy periods: " + numBusyPeriods + ".");

    }

}

class Job {

    public double arrivalTime;
    public double departureTime;
    public double serviceTime;
    public double delayTime;

    // Default constructor
    public Job() {
        arrivalTime = 0.0;
        departureTime = 0.0;
        serviceTime = 0.0;
        delayTime = 0.0;
    }

    // User supplied arguments
    public Job(double aT, double dT, double sT, double deT) {
        arrivalTime = aT;
        departureTime = dT;
        serviceTime = sT;
        delayTime = deT;
    }

    public double getDT() { return departureTime; }

}
