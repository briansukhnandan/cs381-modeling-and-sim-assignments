import java.util.Random;
import java.util.ArrayList;

public class BrianSukhnandan_Proj2 {

    /*

        There are 4 stops in a circle route that a single bus picks up passengers.
        the bus goes one way.

        Passengers that get on, exit before the stop they started at. The bus has a capacity
        of 20 persons. Each arriving passenger requires 2 sec to get on the bus and each
        departing passenger requires 1 sec to get off. The time between all stops half the time
        is 15 sec and the other half, because of traffic, is equally likely to be between 20 to
        40 seconds. People come to one of the four stations with an exponential distribution
        with a mean of every 5 sec.

        Simulate for 10,000 departures from the bus.
        Determine and output the time avg number of passengers on the bus.
        Determine How many times a person has to wait for a round trip of the bus because it
        is full.

        Also for debugging purposes, follow and output the first 20 customers that arrive
        and the 100 through the 120th customer.

     */

    /*
        Brian Sukhnandan
        CS381 - Modeling & Simulation
     */

    public static double getExpDist(int mean) {
        Random rand = new Random();
        return Math.log(1-rand.nextDouble()) / (-1*mean);
    }

    static class Bus_Stop {

        int value;
        Bus_Stop nextStop;
        Bus_Stop prevStop;
        ArrayList<Passenger> waitingPassengers;

        public Bus_Stop(int value, Bus_Stop nextStop, Bus_Stop prevStop, ArrayList<Passenger> waitingPassengers) {
            this.value = value;
            this.nextStop = nextStop;
            this.prevStop = prevStop;
            this.waitingPassengers = waitingPassengers;
        }

        public void setNext(Bus_Stop next) {
            this.nextStop = next;
        }

        public void setPrev(Bus_Stop prev) {
            this.prevStop = prev;
        }

    }

    static class Passenger {

        Bus_Stop stopGotOn;
        Bus_Stop stopGotOff;

        int wait_time;
        double arrival_time;

        public Passenger(Bus_Stop stopGotOn, Bus_Stop stopGotOff) {
            this.stopGotOn = stopGotOn;
            this.stopGotOff = stopGotOff;
            this.wait_time = 0;
            this.arrival_time = 0;
        }

    }

    static class Bus_Stop_Circular_LinkedList {

        public Bus_Stop head;
        public Bus_Stop tail;
        public int size;

        public Bus_Stop_Circular_LinkedList () {

            this.size = 0;
            this.head = null;
            this.tail = null;

        }

        public void appendCircular(Bus_Stop data) {

            if (this.size > 0) {
                this.tail.nextStop = data;
                data.setPrev(this.tail);
                this.tail = data;
                this.tail.setNext(this.head);
            }

            // Executes when size = 0, or the list is empty.
            else {
                this.head = data;
                this.tail = this.head;
            }

            this.size++;
        }

    }

    static class Bus {

        int capacity;
        int spots_left;
        ArrayList<Passenger> listOfPassengers;
        ArrayList<Passenger> passengersGotOnBus;

        Bus_Stop currentStop;
        Bus_Stop_Circular_LinkedList busRoute;

        public Bus(Bus_Stop_Circular_LinkedList busRoute, ArrayList<Passenger> listOfPassengers, ArrayList<Passenger> passengersGotOnBus) {
            this.capacity = 20;
            this.spots_left = this.capacity;
            this.busRoute = busRoute;
            this.listOfPassengers = listOfPassengers;
            this.passengersGotOnBus = passengersGotOnBus;
        }

        public boolean isFull() {
            return this.spots_left == 0;
        }

        public int getTravelTime() {

            Random fR = new Random();

            // Generate random # between 1 and 10.
            int firstRandom = fR.nextInt((10 - 1)+1) + 1;

            // If random num is <= 5 (half) return 15.
            if (firstRandom <= 5) { return 15; }
            else { return fR.nextInt((40 - 20)+1) + 20; }

        }

        public void pickUpPassengers(double time) {

            Random r = new Random();
            int numOfPassengers = r.nextInt( (20 - 1) + 1 ) + 1;

            for (int i = 0; i < numOfPassengers; i++) {

                // Each passenger who gets on at the current stop gets off at the previous
                // stop of the current stop.
                Passenger p = new Passenger(this.currentStop, this.currentStop.prevStop);

                // Each passenger arrives with exponential dist with a mean of 5s.
                time += getExpDist(5);

                // Set passenger's arrival time to current time.
                p.arrival_time = time;

                // If the bus is full.
                if (this.isFull()) {

                    // Add passengers to a future waiting list at current stop.
                    this.currentStop.waitingPassengers.add(p);

                    // Increment wait time for passenger who is added to wait list.
                    p.wait_time += 1;

                }

                // If the bus is not full however.
                else {

                    // If there are people waiting.
                    if ( !(this.currentStop.waitingPassengers.isEmpty() ) ) {

                        // Add passenger at top of waiting list.
                        this.listOfPassengers.add(this.currentStop.waitingPassengers.get(0));

                        this.passengersGotOnBus.add(this.currentStop.waitingPassengers.get(0));

                        // Remove passenger from waiting list.
                        this.currentStop.waitingPassengers.remove(0);

                        // Decrement spots left since a passenger has got on the bus.
                        this.spots_left -= 1;

                    }

                    // If there are no people on the waiting list, put passengers at stop on the bus.
                    else {

                        // Then add that passenger to that list of passengers.
                        this.listOfPassengers.add(p);

                        this.passengersGotOnBus.add(p);

                        // Then decrement the spots left by the number of passengers that got on.
                        this.spots_left -= 1;

                    }

                    // Each passenger requires 2s to get on the bus.
                    time += 2;

                }

            }

        }

        public void letPassengersOff(double time) {

            // Go through the list of passengers
            for (int i = 0; i < this.listOfPassengers.size(); i++) {

                // If any of their stopGotOff = the bus current stop
                if (this.listOfPassengers.get(i).stopGotOff == this.currentStop) {

                    // Remove that passenger from listOfPassengers.
                    this.listOfPassengers.remove(i);

                    // Increment spots left on the bus.
                    this.spots_left += 1;

                    // Each passenger requires 1s to get off bus.
                    time += 1;

                }

            }

        }

    }

    public static void startSimulation(Bus b, int departures) {

        // Initialize time at 0.
        double time = 0.0;

        // Keep a counter for number of passengers that exited bus.
        int numberOfPassengersGottenOff = 0;

        // Set current stop before loop iterations to first bus stop.
        // First bus stop is head of Linked List.
        b.currentStop = b.busRoute.head;

        for (int i = 0; i < departures; i++) {

            // If the bus is not full,
            if ( !(b.isFull()) ) {
                // Pick up passengers.
                b.pickUpPassengers(time);

            }

            // If the bus is full however
            else {

                // Let passengers off if they are at stop before one they got on.
                // Then continue to next iteration.
                b.letPassengersOff(time);

            }

            // There is a 50/50 chance the travel time between stops will be 15s and (20-40)s.
            // Before bus makes another departure to another stop, increment time by travel time.
            time += b.getTravelTime();

            // Also go to the next stop.
            b.currentStop = b.currentStop.nextStop;
        }

    }

    public static void main(String args[]) {

        System.out.println("\nBrian Sukhnandan");
        System.out.println("CS381 - Modeling & Simulation");

        // Initialize Doubly Linked list of bus_stops and initialize previous and next nodes.
        Bus_Stop_Circular_LinkedList Bus_List = new Bus_Stop_Circular_LinkedList();

        Bus_Stop stopOne = new Bus_Stop(1, null, null, new ArrayList<Passenger>());
        Bus_Stop stopTwo = new Bus_Stop(2, null, stopOne, new ArrayList<Passenger>());
        Bus_Stop stopThree = new Bus_Stop(3, null, stopTwo, new ArrayList<Passenger>());
        Bus_Stop stopFour = new Bus_Stop(4, null, stopThree, new ArrayList<Passenger>());

        stopOne.setPrev(stopFour);
        stopOne.setNext(stopTwo);
        stopTwo.setNext(stopThree);
        stopThree.setNext(stopFour);
        stopFour.setNext(stopOne);

        Bus_List.appendCircular(stopOne);
        Bus_List.appendCircular(stopTwo);
        Bus_List.appendCircular(stopThree);
        Bus_List.appendCircular(stopFour);

        ArrayList<Passenger> listOfPassengers = new ArrayList<Passenger>();
        ArrayList<Passenger> passengersGotOnBus = new ArrayList<Passenger>();

        // Create a new bus with capacity 20.
        Bus bus = new Bus(Bus_List, listOfPassengers, passengersGotOnBus);

        // Start simulation for 10,000 departures.
        startSimulation(bus, 10000);

        System.out.println("\n");

        for (int i = 0; i < 20; i++) {
            System.out.println("Customer "+(i+1)+":");
            System.out.println("Arrival time: "+bus.passengersGotOnBus.get(i).arrival_time+" seconds.");
            System.out.println("# of round trips they have to wait: "+bus.passengersGotOnBus.get(i).wait_time);
            System.out.println();
        }

        for (int i = 99; i < 120; i++) {
            System.out.println("Customer "+(i+1)+":");
            System.out.println("Arrival time: "+bus.passengersGotOnBus.get(i).arrival_time);
            System.out.println("# of round trips they have to wait: "+bus.passengersGotOnBus.get(i).wait_time);
            System.out.println();
        }

    }

}
