import java.util.Random;

// Brian Sukhnandan
// CS381 - Modeling and Simulation

public class Chebyshev_BrianSukhnandan {

    public static int computeChebyshev(int n) { return (int)((1 - (1 / Math.pow(n,2)))*100); }

    public static void main(String args[]) {

        int[] arrayOfRand = new int[100];
        Random r = new Random();

        for (int i = 0; i < 100; i++)
            // Generate rand numbers range 1-100.
            arrayOfRand[i] = r.nextInt(10) + 1;

        // We will assume x-bar is 1, for simplicity, and use mean = 5.
        int twosCounter = 0;
        int threesCounter = 0;
        for (int i = 0; i < 100; i++) {
            if (arrayOfRand[i] >= 3 && arrayOfRand[i] <= 7) {
                twosCounter++;
            }
            if (arrayOfRand[i] >= 2 && arrayOfRand[i] <= 8) {
                threesCounter++;
            }
        }

        System.out.println("Using Chebyshev's inequality for interval 2s: "+computeChebyshev(2)+"/100");
        System.out.println("Actual proportion of sample interval 2s: "+twosCounter+"/100\n");

        System.out.println("Using Chebyshev's inequality for interval 3s: "+computeChebyshev(3)+"/100");
        System.out.println("Actual proportion of sample interval 3s: "+threesCounter+"/100\n");


        // System.out.println("Hello World");

    }

}
